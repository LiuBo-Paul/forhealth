//
//  MessageClassTablemodel.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageClassTablemodel : NSObject
@property (nonatomic,copy) NSString *name;                   //分类名
@property (nonatomic,copy, setter=setIds:) NSString *ids;    //分类号
@end
