//
//  MessageSearchModel.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "MessageSearchModel.h"

@implementation MessageSearchModel
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if([key isEqualToString:@"id"])
    {
        _ids = [NSString stringWithFormat:@"%@",value];
    }
}
-(void)setIds:(NSString *)ids
{
    _ids = ids;
}
@end
