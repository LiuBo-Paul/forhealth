//
//  MessageTableModel.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageTableModel : NSObject
@property (nonatomic,copy) NSString *title;                //标题
@property (nonatomic,copy) NSString *img;                  //图片
@property (nonatomic,copy) NSString *count;                //访问量
@property (nonatomic,copy) NSString *author;               //作者
@property (nonatomic,copy) NSString *loreclass;            //未知
@property (nonatomic,copy) NSString *className;            //所在分类名
@property (nonatomic,copy) NSString *md;                   //未知
@property (nonatomic,copy) NSString *time;                 //上传时间
@property (nonatomic,copy, setter=setIds:) NSString *ids;  //分类号
@end
