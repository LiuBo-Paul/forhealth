//
//  MessageClassTablemodel.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "MessageClassTablemodel.h"

@implementation MessageClassTablemodel
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if([key isEqualToString:@"id"])
    {
        _ids = value;
    }
}
-(void)SetIds:(NSString *)ids
{
    _ids = ids;
}
@end
