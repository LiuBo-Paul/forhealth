//
//  MessageSearchModel.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageSearchModel : NSObject
@property (nonatomic,copy,setter=setIds:) NSString *ids;             //编号
@property (nonatomic,copy) NSString *title;           //标题
@property (nonatomic,copy) NSString *img;             //图片
@property (nonatomic,copy) NSString *content;         //内容
@property (nonatomic,copy) NSString *type;            //类型
@end