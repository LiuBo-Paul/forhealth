//
//  TableViewCell.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "TableViewCell.h"
#import "UIImageView+WebCache.h"
#import "DealWebString.h"
#import "DBManager.h"
@implementation TableViewCell

- (void)awakeFromNib {
}
-(void)setTableViewCell:(MessageTableModel *)model
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.yi18.net/%@",model.img]]];
        _iconImageView.clipsToBounds = YES;
        _iconImageView.layer.cornerRadius = 10;
    });
    [_titleLable setText:[DealWebString getStringFromWebString:model.title]];
    _titleLable.clipsToBounds = YES;
    _titleLable.layer.cornerRadius = 10;
    _typeLable.text = [NSString stringWithFormat:@"%@", model.className];
    _countLable.text = [NSString stringWithFormat:@"%@", model.count];
    _authorLable.text = [NSString stringWithFormat:@"%@", model.author];
    BOOL ret = [[DBManager sharedManager] isAppExists:model.ids];
    if(ret)
    {
        _favorite.hidden = NO;
    }
    else
    {
        _favorite.hidden = YES;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
