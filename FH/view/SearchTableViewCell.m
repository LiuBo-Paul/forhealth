//
//  SearchTableViewCell.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "SearchTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "DealWebString.h"
#import "DBManager.h"
@implementation SearchTableViewCell
- (instancetype)init
{
    self = [super init];
    if (self) {
        NSInteger xP = 10, yP = 5, imgW = 50, imgH = 50, labH = 20, space = 10,winW = [UIScreen mainScreen].bounds.size.width,winH = 100;
        [self setBackgroundColor:[UIColor lightTextColor]];
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(2, 2, [UIScreen mainScreen].bounds.size.width-4,winH-4)];
        backView.clipsToBounds = YES;
        backView.layer.cornerRadius = 15;
        backView.layer.borderColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.2 alpha:0.2].CGColor;
        backView.layer.borderWidth = 0.8;
        [backView setBackgroundColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0.3 alpha:0.1]];
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(xP,yP,winW-xP*2-30,labH)];
        [_titleLable setTextAlignment:NSTextAlignmentLeft];
        _favorite = [[UIImageView alloc] initWithFrame:CGRectMake(_titleLable.frame.size.width, yP, 15, 15)];
        _favorite.image = [UIImage imageNamed:@"icon_favrite"];
        _favorite.hidden = YES;
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(winW-imgW-space ,yP+space+labH,imgW,imgH)];
        _iconImageView.clipsToBounds = YES;

        _iconImageView.layer.cornerRadius = 10;
        _contentLable = [[UILabel alloc] initWithFrame:CGRectMake(xP,yP+labH,winW-xP-space-imgW-space*2,winH - (xP + space + labH))];
        [_contentLable setFont:[UIFont systemFontOfSize:12]];
        [_contentLable setTextColor:[UIColor lightGrayColor]];
        _titleLable.clipsToBounds = YES;
        _titleLable.layer.cornerRadius = 2;
        _contentLable.clipsToBounds = YES;
        _contentLable.layer.cornerRadius = 10;
        _contentLable.numberOfLines = 0;
        _contentLable.lineBreakMode = NSLineBreakByWordWrapping;
        [backView addSubview:_titleLable];
        [backView addSubview:_contentLable];
        [backView addSubview:_iconImageView];
        [backView addSubview:_favorite];
        [self addSubview:backView];
    }
    return self;
}
-(void)setSearchCell:(MessageSearchModel *)model
{
    [_titleLable setText:[DealWebString getStringFromWebString: model.title]];
    [_contentLable setText:[DealWebString getStringFromWebString: model.content]];
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.yi18.net/%@",model.img]]];
    BOOL ret = [[DBManager sharedManager] isAppExists:model.ids];
    if(ret)
    {
        _favorite.hidden = NO;
    }
}
- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
