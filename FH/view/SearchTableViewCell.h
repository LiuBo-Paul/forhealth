//
//  SearchTableViewCell.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageSearchModel.h"
@interface SearchTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *titleLable;
@property (nonatomic,strong) UILabel *deatilLable;
@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) UILabel *contentLable;
@property (nonatomic,strong) NSString *num;
@property (nonatomic,strong) UIImageView *favorite;
-(void)setSearchCell:(MessageSearchModel *)model;
@end
