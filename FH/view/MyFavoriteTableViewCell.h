//
//  MyFavoriteTableViewCell.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/15.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDetailModel.h"

@interface MyFavoriteTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) IBOutlet UILabel *classNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
-(void)setMyFavoriteCell:(MessageDetailModel *)model;
@end
