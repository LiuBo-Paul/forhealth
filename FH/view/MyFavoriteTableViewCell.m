//
//  MyFavoriteTableViewCell.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/15.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "MyFavoriteTableViewCell.h"
#import "DealWebString.h"
@implementation MyFavoriteTableViewCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)setMyFavoriteCell:(MessageDetailModel *)model
{
    [_titleLable setText:[DealWebString getStringFromWebString:model.title]];
    _titleLable.clipsToBounds = YES;
    _titleLable.layer.cornerRadius = 10;
    _classNameLabel.text = model.className;
    _countLabel.text = model.count;
    _timeLabel.text = model.time;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
