//
//  DealWebString.m
//  ForHealth
//
//  Created by qianfeng on 15/10/18.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "DealWebString.h"

@implementation DealWebString
+(NSString *)getStringFromWebString:(NSString *)webString
{
    NSString *string = webString;
    NSMutableString *retString = [[NSMutableString alloc] init];
    int n = (int)string.length;
    for (int i=0; i<n; i++)
    {
        NSRange range = {i,1};
        NSString *cha = [string substringWithRange:range];
        if([cha isEqualToString:@"<"])
        {
            int endIndex = i;
            i=i+1;
            while (1)
            {
                NSRange range1 = {endIndex,1};
                NSString *beg = [string substringWithRange:range1];
                if([beg isEqualToString:@">"])
                {
                    i = endIndex;
                    break;
                }
                else
                {
                    endIndex++;
                }
            }
        }
        else
        {
            [retString appendString:cha];
        }
    }
    return retString;
}
@end
