//
//  DBManager.h
//  LimitFreeApp1514
//
//  Created by weikejun on 15/9/30.
//  Copyright (c) 2015年 com.qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "MessageDetailModel.h"
@interface DBManager : NSObject
+(id)sharedManager;
//添加数据
-(void)insertFavoriteModel:(MessageDetailModel *)model;
//根据应用的id删除该应用的记录
-(void)deleteDataById:(NSString *)applicationId;
//获取所有的应用信息
-(NSArray*)fetchAllData;
//根据应用的id判断是否存在
-(BOOL)isAppExists:(NSString*)applicationId;
@end
