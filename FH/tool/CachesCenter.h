//
//  CachesCenter.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#define PATH [NSString stringWithFormat:@"%@/Documents/Caches",NSHomeDirectory()]
@interface CachesCenter : NSObject
@property (nonatomic, copy) NSData *cachedData;
@property (nonatomic, copy) NSString *cacheFileName;
-(NSString *)getHomePathByCacheFileName:(NSString *)cacheFileName;
-(void)saveData:(NSData *)data WithFileName:(NSString *)fileName;
-(NSData *)getDataByFileName:(NSString *)fileName;
-(BOOL)isExistFile:(NSString *)fileName;
-(NSMutableDictionary *)fechAllFileData;
-(NSString *)clearCaches;
-(NSInteger)getAllCachesSize;
@end
