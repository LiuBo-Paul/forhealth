//
//  CachesCenter.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "CachesCenter.h"
@interface CachesCenter()
{
    NSFileManager *_manager;
}
@end
@implementation CachesCenter
- (instancetype)init
{
    self = [super init];
    if (self) {
        if(!_manager)
        {
            _manager = [[NSFileManager alloc] init];
            NSString *path = [NSString stringWithFormat:@"%@",PATH];
            BOOL ret = [_manager fileExistsAtPath:path];
            if(!ret)
            {
                [_manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
            }
        }
    }
    return self;
}
//获取缓存"根路径+文件名"
-(NSString *)getHomePathByCacheFileName:(NSString *)cacheFileName
{
    NSString *path = [NSString stringWithFormat:@"%@/%@",PATH,cacheFileName];
    return path;
}
//保存文件
-(void)saveData:(NSData *)data WithFileName:(NSString *)fileName
{
    NSString *homePath = [self getHomePathByCacheFileName:fileName];
    BOOL ret = [data writeToFile:homePath atomically:YES];
    if(ret)
    {
        NSLog(@"data save success!");
    }
    else
    {
        NSLog(@"data save fail!");
    }
}
//读取文件
-(NSData *)getDataByFileName:(NSString *)fileName
{
    NSString *homePath = [self getHomePathByCacheFileName:fileName];
    BOOL ret = [self isExistFile:fileName];
    if(ret)
    {
        NSData *data = [[NSData alloc] initWithContentsOfFile:homePath];
        if(data.length>0)
        {
            return data;
        }
        else
        {
            NSLog(@"file is Empty!");
            return nil;
        }
    }
    else
    {
        NSLog(@"file isn`t exist!");
        return nil;
    }
}
//判断是否存在文件
-(BOOL)isExistFile:(NSString *)fileName
{
    NSString *homePath = [self getHomePathByCacheFileName:fileName];
    _manager = [[NSFileManager alloc] init];
    BOOL ret = [_manager fileExistsAtPath:homePath isDirectory:nil];
    return ret;
}
//获取所有文件数据
-(NSMutableDictionary *)fechAllFileData
{
    NSMutableDictionary *dict = nil;
    NSFileManager *manager = [[NSFileManager alloc] init];
    NSMutableArray *fileNameArray = [[NSMutableArray alloc] init];
    NSMutableArray *fileDataArray = [[NSMutableArray alloc] init];
    NSArray *array = [manager contentsOfDirectoryAtPath:PATH error:nil];
    for (NSString *fileName in array)
    {
        if(![[fileName substringToIndex:1] isEqualToString:@"."])
        {
            NSString *homPath = [self getHomePathByCacheFileName:fileName];
            NSData *data = [[NSData alloc] initWithContentsOfFile:homPath];
            [fileNameArray addObject:fileName];
            [fileDataArray addObject:data];
            NSLog(@"fileName:%@",fileName);
        }
    }
    if(fileNameArray.count == fileDataArray.count)
    {
        dict = [[NSMutableDictionary alloc] initWithObjects:fileDataArray forKeys:fileNameArray];
    }
    return dict;
}
//清空缓存
-(NSString *)clearCaches
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[self fechAllFileData]];
    if ([dic allKeys].count>0)
    {
        for (NSString *filename in [dic allKeys])
        {
            NSString *homePath = [self getHomePathByCacheFileName:filename];
            NSFileManager *manager = [[NSFileManager alloc] init];
            [manager removeItemAtPath:homePath error:nil];
        }
        return @"清理完成!";
    }
    else
    {
        return @"暂无缓存文件!";
    }
}
//获取缓存大小
-(NSInteger)getAllCachesSize
{
    NSFileManager *manager = [[NSFileManager alloc] init];
    //单位是bit
    return (NSInteger)[[manager attributesOfItemAtPath:PATH error:nil] fileSize];
}
@end
