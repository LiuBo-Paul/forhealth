//
//  DealWebString.h
//  ForHealth
//
//  Created by qianfeng on 15/10/18.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealWebString : NSObject
+(NSString *)getStringFromWebString:(NSString *)webString;
@end
