//
//  RequestDataCenter.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestDataCenter : NSObject
@property (nonatomic, copy) NSString *urlString;
+(id)sharedRequest;
-(void)getDataFromUrlString:(NSString *)urlString;
@end
