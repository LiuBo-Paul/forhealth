//
//  RequestDataCenter.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "RequestDataCenter.h"
#define MYKEY @"84c0b7f21bfe462d53787895f7e4797e"
@interface RequestDataCenter()<NSURLConnectionDataDelegate>
{
    NSURLConnection *_urlConnection;
    NSMutableData *_receiveData;
}
@end
@implementation RequestDataCenter
+(id)sharedRequest
{
    RequestDataCenter *requestDataCenter = nil;
    if(requestDataCenter == nil)
    {
        requestDataCenter = [[RequestDataCenter alloc] init];
    }
    return requestDataCenter;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _receiveData = [[NSMutableData alloc] init];
    }
    return self;
}
-(void)getDataFromUrlString:(NSString *)urlString
{
    NSDictionary *headers = @{ @"accept": @"application/json",
                               @"content-type": @"application/json",
                               @"apix-key": [MYKEY stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    _urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if(error)
    {
        NSLog(@"error:%@",error.localizedDescription);
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(_receiveData != nil)
    {
        [_receiveData appendData:data];
    }
    else
    {
        NSLog(@"_receiveData未进行初始化!");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if(_receiveData != nil)
    {
        [_receiveData setLength:0];
    }
    else
    {
        NSLog(@"_receiveData未进行初始化!");
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadFinished" object:nil userInfo:@{@"data":_receiveData}];
}
@end
