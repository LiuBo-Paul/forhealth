//
//  DBManager.m
//  LimitFreeApp1514
//
//  Created by weikejun on 15/9/30.
//  Copyright (c) 2015年 com.qianfeng. All rights reserved.
//

#import "DBManager.h"
#import "UIImageView+WebCache.h"
@implementation DBManager
{
    FMDatabase *_db;
}
+(id)sharedManager
{
    static DBManager *manager=nil;
    @synchronized(self){
        if(manager==nil){
            manager=[[DBManager alloc]init];
        }
    }
    return manager;
}

-(instancetype)init
{
    if(self=[super init]){
        NSString *path=[NSHomeDirectory() stringByAppendingPathComponent:@"/Documents/detail.db"];
        NSLog(@"path:%@",path);
        _db=[[FMDatabase alloc]initWithPath:path];
        if([_db open]){
            NSString *sql=@"create table if not exists collect(id integer primary key autoincrement,applicationId varchar(255),title varchar(255),message varchar(255),count varchar(255),author varchar(255),loreclass varchar(255),className varchar(255),time varchar(255),ids varchar(255))";
            BOOL ret=[_db executeUpdate:sql];
            if(!ret){
                NSLog(@"create table error:%@",_db.lastErrorMessage);
            }
        }else{
            NSLog(@"open database error!");
        }
    }
    return self;
}

-(void)insertFavoriteModel:(MessageDetailModel *)model
{
    NSString *sql=@"insert into collect(title,message,count,author,loreclass,className,time,ids)values(?,?,?,?,?,?,?,?)";
    BOOL ret=[_db executeUpdate:sql,
              [NSString stringWithFormat:@"%@", model.title],
              [NSString stringWithFormat:@"%@", model.message],
              [NSString stringWithFormat:@"%@", model.count],
              [NSString stringWithFormat:@"%@", model.author],
              [NSString stringWithFormat:@"%@", model.loreclass],
              [NSString stringWithFormat:@"%@", model.className],
              [NSString stringWithFormat:@"%@", model.time],
              [NSString stringWithFormat:@"%@", model.ids]];
    if(!ret)
    {
        NSLog(@"insert error:%@",_db.lastErrorMessage);
    }
}
-(void)deleteDataById:(NSString *)ids
{
    NSString *sql=@"delete from collect where ids=?";
    BOOL ret=[_db executeUpdate:sql,ids];
    if(!ret){
        NSLog(@"delete error:%@",_db.lastErrorMessage);
    }
}
-(NSArray*)fetchAllData
{
    NSMutableArray *array=[[NSMutableArray alloc]init];
    NSString *sql=@"select * from collect";
    FMResultSet *rs=[_db executeQuery:sql];
    while ([rs next])
    {
        MessageDetailModel *model=[[MessageDetailModel alloc]init];
        model.ids=[rs stringForColumn:@"applicationId"];
        model.title=[rs stringForColumn:@"title"];
        model.message = [rs stringForColumn:@"message"];
        model.count=[rs stringForColumn:@"count"];
        model.author = [rs stringForColumn:@"author"];
        model.loreclass=[rs stringForColumn:@"loreclass"];
        model.className = [rs stringForColumn:@"className"];
        model.time=[rs stringForColumn:@"time"];
        model.ids = [rs stringForColumn:@"ids"];
        [array addObject:model];
    }
    return array;
}
-(BOOL)isAppExists:(NSString*)ids
{
    NSString *sql=@"select * from collect where ids=?";
    FMResultSet *rs=[_db executeQuery:sql,ids];
    return [rs next];
}
@end
