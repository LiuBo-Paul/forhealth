//
//  OtherViewController.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/13.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "OtherViewController.h"
#import "MyFavoriteViewController.h"
#import "AboutViewController.h"
#import "CachesCenter.h"
#import "ClearAnimationViewController.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface OtherViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_titleArray;
    NSMutableArray *_imageArray;
    CachesCenter *_caches;
}
@end

@implementation OtherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    UINavigationItem *item = self.navigationItem;
    UIImageView *titleView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleView.clipsToBounds = YES;
    titleView.layer.cornerRadius = 10;
    titleView.image = [UIImage imageNamed:@"titleBackGround"];
    UILabel *titleLable = [[UILabel alloc] initWithFrame:titleView.frame];
    [titleLable setTextAlignment:NSTextAlignmentCenter];
    [titleLable setText:@"我的设置"];
    [titleView addSubview:titleLable];
    
    [item setTitleView:titleView];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _titleArray = [[NSMutableArray alloc] init];
    _imageArray = [[NSMutableArray alloc] init];
    _caches = [[CachesCenter alloc] init];
    [self creatData];
    [self creatUI];
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.tabBarController.tabBar.hidden = NO;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.tabBarController.tabBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.tabBarController.tabBar.hidden = YES;
}
-(void)creatUI
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64-49)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
}
-(void)creatData
{
    NSArray *arrayTitle = @[@"收藏的分类",@"清空缓存",@"关于"];
    [_titleArray addObjectsFromArray:arrayTitle];
    NSArray *arrayImage = @[@"image_favorit",@"image_clear",@"image_about"];
    [_imageArray addObjectsFromArray:arrayImage];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titleArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = _titleArray[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:_imageArray[indexPath.row]];
    cell.tag = 100+indexPath.row;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        MyFavoriteViewController *myFavorite = [[MyFavoriteViewController alloc] init];
        
        [self.navigationController pushViewController:myFavorite animated:YES];
    }
    else if(indexPath.row == 1)
    {
        NSInteger size = [_caches getAllCachesSize];
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"缓存"] message:[NSString stringWithFormat:@"缓存大小:%d%@", size>(1024*1024)?size/1024/1024:(size>1024?size/1024:size), size>(1024*1024)?@"MB":(size>1024?@"KB":@"B")] preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:@"清空" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [_tableView reloadData];
        }]];
        [controller addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [_tableView reloadData];
        }]];
        ClearAnimationViewController *subView = [[ClearAnimationViewController alloc] init];
        [controller addChildViewController:subView];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else if(indexPath.row == 2)
    {
        AboutViewController *about = [[AboutViewController alloc] init];
        
        [self.navigationController pushViewController:about animated:YES];
    }
}

@end
