//
//  ClassesTableViewController.h
//  FH
//
//  Created by qianfeng on 15/10/20.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchViewController.h"
@interface ClassesTableViewController : UIViewController
@property (nonatomic,strong) void(^callback)(SearchViewController *searViewController, NSString *keyWord,NSString *page);
@property (nonatomic,strong)void(^swipBack)();
@end
