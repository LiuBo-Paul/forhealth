//
//  RootTableViewController.h
//  FH
//
//  Created by qianfeng on 15/10/20.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootTableViewController : UIViewController
@property (nonatomic,copy) __block NSString *page;
@property (nonatomic,copy) NSString *limit;
@property (nonatomic,copy) NSString *type;
@end
