//
//  ClearAnimationViewController.m
//  ForHealth
//
//  Created by qianfeng on 15/10/18.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "ClearAnimationViewController.h"

@interface ClearAnimationViewController ()

@end

@implementation ClearAnimationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setFrame:CGRectMake(0, 0, 100, 20)];
    [self.view setBackgroundColor:[UIColor redColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
