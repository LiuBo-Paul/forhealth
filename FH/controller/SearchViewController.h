//
//  SearchViewController.h
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController
@property (nonatomic,copy) NSString *keyWord;
@property (nonatomic,copy) NSString *page;
@end
