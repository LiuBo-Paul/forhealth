//
//  AboutViewController.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/15.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
@end
#define WIDTH [UIScreen  mainScreen].bounds.size.width
#define HEIGHT [UIScreen  mainScreen].bounds.size.height

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backClick:)];
    UILabel *titleLable = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH-200)/2, 100, 200, 30)];
    [titleLable setText:@"关于健康咨询"];
    [titleLable setTextAlignment:1];
    [titleLable setFont:[UIFont systemFontOfSize:25]];
    [titleLable setBackgroundColor:[UIColor lightTextColor]];
    NSString *str = @"    健康资讯为您提供各类有关健康的讯息,从儿童到老人,从日常饮食到健康的生活习惯,这里都有你想了解的.";
    UILabel *briefLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, WIDTH-20 , 200)];
    [briefLabel setText:str];
    [briefLabel setFont:[UIFont systemFontOfSize:15]];
    briefLabel.numberOfLines = 0;
    [briefLabel setContentMode:UIViewContentModeTop];
    [briefLabel setTextAlignment:0];
    [self.view addSubview:titleLable];
    [self.view addSubview:briefLabel];
}
-(void)backClick:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
