//
//  DetailViewController.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "DetailViewController.h"
#import "RequestDataCenter.h"
#import "MessageDetailModel.h"
#import "CachesCenter.h"
#import "DBManager.h"
@interface DetailViewController ()
{
    UILabel *_titleLable;
    UILabel *_messageLable;
//    UILabel *_timeLable;
    UIWebView *_webView;
    MessageDetailModel *_model;
    CachesCenter *_caches;
    BOOL _isFirstTime;
    NSTimer *_timer;
    UIActivityIndicatorView *_indicator;
    BOOL _isNetConnection;
    UILabel *_noWebLable;
}
@end
#define URL @"http://a.apix.cn/yi18/lore/show?id=%@"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    UINavigationItem *item = self.navigationItem;
    [item setTitle:@"详情"];
    _indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((WIDTH-40)/2, (HEIGHT-100)/2, 40, 40)];
    [self.view addSubview:_indicator];
    _indicator.hidden = NO;
    [_indicator setColor:[UIColor lightGrayColor]];
    [_indicator startAnimating];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self start];
    _isNetConnection = YES;
    _isFirstTime = YES;
    _timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(showDetail) userInfo:nil repeats:YES];
    BOOL ret = [[DBManager sharedManager] isAppExists:_model.ids];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:ret==YES?@"已收藏":@"收藏" style:UIBarButtonItemStyleDone target:self action:@selector(rightButtonClick:)];
    
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)rightButtonClick:(UIBarButtonItem *)sender
{
    if([sender.title isEqualToString:@"收藏"])
    {
        [[DBManager sharedManager] insertFavoriteModel:_model];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"收藏提示" message:@"本文已经收藏成功,到\"我的收藏\"里可以查看哦!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        [self updataBar];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"收藏提示" message:@"本文已经在\"我的收藏\"里!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    }
}
-(void)updataBar
{
    BOOL ret = [[DBManager sharedManager] isAppExists:_model.ids];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:ret==YES?@"已收藏":@"收藏" style:UIBarButtonItemStyleDone target:self action:@selector(rightButtonClick:)];
}
-(void)start
{
    _caches = [[CachesCenter alloc] init];
    NSData *data = [[NSData alloc] initWithData:[_caches getDataByFileName:_classId]];
    if(data.length != 0)
    {
        NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if(dic1)
        {
            _model = [[MessageDetailModel alloc] init];
            [_model setValuesForKeysWithDictionary:dic1[@"yi18"]];
            [_indicator stopAnimating];
            [_indicator removeFromSuperview];
            [self creatUI];
            [self updateUI];
        }
        else
        {
            NSLog(@"缓存数据有问题!");
        }
    }
    else
    {
        [self requestData];
    }
}
-(void)creatUI
{
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.1]];
    _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 70, WIDTH, 50)];
    [_titleLable setTextColor:[UIColor blackColor]];
    [_titleLable setBackgroundColor:[UIColor whiteColor]];
    [_titleLable setNumberOfLines:3];
    [_titleLable setTextAlignment:NSTextAlignmentCenter];
    [_titleLable adjustsFontSizeToFitWidth];
    _titleLable.numberOfLines = 0;
    _titleLable.userInteractionEnabled = YES;
    [_titleLable setBackgroundColor:[UIColor whiteColor]];
    
    _messageLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, WIDTH, 20)];
    [_messageLable setText:[NSString stringWithFormat:@"来自:%@ 访问量:%@ 分类:%@",_model.author,_model.count,_model.className]];
    [_messageLable setAlpha:0];
    [_messageLable setTextAlignment:NSTextAlignmentCenter];
    [_messageLable setFont:[UIFont systemFontOfSize:10]];
    [_messageLable setBackgroundColor:[UIColor whiteColor]];
    UIView *viewOfWeb = [[UIView alloc] initWithFrame:CGRectMake(0, 140, WIDTH, HEIGHT-130-10)];
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, viewOfWeb.frame.size.width, viewOfWeb.frame.size.height-5)];
    _webView.clipsToBounds = NO;
    _webView.layer.cornerRadius = 0;
    [viewOfWeb addSubview:_webView];
    [self.view addSubview:_titleLable];
    [self.view addSubview:_messageLable];
    [self.view addSubview:viewOfWeb];
}
-(void)showDetail
{
    static int i=0;
    [UIView animateWithDuration:2 animations:^{
        if(i%2)
        {

            [_messageLable setText:[NSString stringWithFormat:@"来自:%@ 访问量:%@ 分类:%@",_model.author,_model.count,_model.className]];
            [_messageLable setAlpha:1];
        }
        else
        {
            [_messageLable setText:[NSString stringWithFormat:@"上传时间:%@",_model.time]];
            [_messageLable setAlpha:1];
        }

    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2 animations:^{
            [_messageLable setAlpha:0];
        }];
        i++;
    }];

}
-(void)updateUI
{
    _titleLable.text = _model.title;
    [_webView loadHTMLString:_model.message baseURL:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _isFirstTime = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadFinished" object:nil];
}
-(void)requestData
{
    [[RequestDataCenter sharedRequest] getDataFromUrlString:[NSString stringWithFormat:URL,_classId]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveData:) name:@"downloadFinished" object:nil];
    NSTimer *timer = [[NSTimer alloc] init];
    timer  = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(isNet) userInfo:nil repeats:NO];
}
-(void)isNet
{
    if(_isNetConnection)
    {
        [_indicator stopAnimating];
        _indicator.hidden = YES;
        [_indicator removeFromSuperview];
    }
    else
    {
        _noWebLable = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH-200)/2, (HEIGHT-64-49-20)/2, 200, 20)];
        [_indicator removeFromSuperview];
        [_noWebLable setTextAlignment:NSTextAlignmentCenter];
        [_noWebLable setText:@"亲!网络出问题了!"];
        [self.view addSubview:_noWebLable];
    }
}
-(void)receiveData:(NSNotification *)notify
{
    _isNetConnection = YES;
    if(_isFirstTime)
    {
        [_indicator stopAnimating];
        _indicator.hidden = YES;
        [_indicator removeFromSuperview];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic = [NSMutableDictionary dictionaryWithDictionary:notify.userInfo];
        [_caches saveData:dic[@"data"] WithFileName:[NSString stringWithFormat:@"%@", _classId]];
        NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:dic[@"data"] options:NSJSONReadingMutableContainers error:nil];
        if(dic1)
        {
            _model = [[MessageDetailModel alloc] init];
            [_model setValuesForKeysWithDictionary:dic1[@"yi18"]];
            [self creatUI];
            [self updateUI];
        }
        else
        {
            NSLog(@"接收数据有问题!");
        }
        _isFirstTime = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
