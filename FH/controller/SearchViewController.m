//
//  SearchViewController.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/12.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "SearchViewController.h"
#import "RequestDataCenter.h"
#import "SearchTableViewCell.h"
#import "MessageDetailModel.h"
#import "CachesCenter.h"
#import "DetailViewController.h"

#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface SearchViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    CachesCenter *_caches;
    BOOL _isFirstTime;
    UIActivityIndicatorView *_indicator;
    NSInteger _selectedIndex;
}
@end
#define URL @"http://a.apix.cn/yi18/lore/search?keyword=%@&page=%@"
@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _selectedIndex = -1;
    self.automaticallyAdjustsScrollViewInsets = NO;
    UINavigationItem *item = self.navigationItem;
    [item setTitle:[NSString stringWithFormat:@"\"%@\"的搜索结果",_keyWord]];
    _caches = [[CachesCenter alloc] init];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backClick:)]];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    _dataArray = [[NSMutableArray alloc] init];
    _isFirstTime = YES;
    [self creatUI];
    if(_dataArray.count == 0)
    {
        NSData *data = [[NSData alloc] initWithData:[_caches getDataByFileName:[NSString stringWithFormat:@"%@%@",_keyWord,_page]]];
        if(data.length != 0)
        {
            NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if(dic1)
            {
                _tableView.hidden = NO;
                [_indicator stopAnimating];
                [_indicator removeFromSuperview];
                NSArray *array = (NSArray *)dic1[@"yi18"];
                for (NSDictionary *dic in array)
                {
                    MessageSearchModel *model = [[MessageSearchModel alloc] init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_dataArray addObject:model];
                }
                [_tableView reloadData];
            }
            else
            {
                NSLog(@"缓存数据有问题!");
            }
        }
        else
        {
            [self requestData];
        }
    }
}
-(void)backClick:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)creatUI
{
    _indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((WIDTH-40)/2, (HEIGHT-100)/2, 40, 40)];
    [self.view addSubview:_indicator];
    _indicator.hidden = NO;
    [_indicator setColor:[UIColor lightGrayColor]];
    [_indicator startAnimating];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.hidden = YES;
    [_tableView setSeparatorColor:[UIColor clearColor]];
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 20)];
    [lable setText:@"没有更多搜索结果了,试试其他关键字吧!"];
    [lable setTextAlignment:NSTextAlignmentCenter];
    [lable setFont:[UIFont systemFontOfSize:15]];
    [lable setTextColor:[UIColor colorWithHue:1.0 saturation:0.5 brightness:1.0 alpha:1.0]];
    _tableView.tableFooterView = (UIView *)lable;
    [_tableView setSeparatorColor:[UIColor clearColor]];
    [self.view addSubview:_tableView];
    [self.view addSubview:_indicator];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadFinished" object:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(_selectedIndex == -1)
    {
        [_tableView reloadData];
    }
    else
    {
        NSIndexPath *indexpath = [NSIndexPath indexPathForItem:_selectedIndex inSection:0];
        [_tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}
-(void)requestData
{
    [[RequestDataCenter sharedRequest] getDataFromUrlString:[[NSString stringWithFormat:URL,_keyWord,_page] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveData:) name:@"downloadFinished" object:nil];
}
-(void)receiveData:(NSNotification *)notify
{
    if(_isFirstTime)
    {
        _tableView.hidden = NO;
        [_indicator stopAnimating];
        [_indicator removeFromSuperview];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic = [NSMutableDictionary dictionaryWithDictionary:notify.userInfo];
        NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:dic[@"data"] options:NSJSONReadingMutableContainers error:nil];
        if(dic1)
        {
            NSArray *array = (NSArray *)dic1[@"yi18"];
            for (NSDictionary *dic in array)
            {
                MessageSearchModel *model = [[MessageSearchModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [_dataArray addObject:model];
            }
            [_tableView reloadData];
        }
        else
        {
            NSLog(@"接收数据有问题!");
        }
        _isFirstTime = NO;
        [_caches saveData:dic[@"data"] WithFileName:[NSString stringWithFormat:@"%@%@",_keyWord,_page]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[SearchTableViewCell alloc] init];
    }
    MessageSearchModel *model = [[MessageSearchModel alloc] init];
    model = _dataArray[indexPath.row];
    if(model)
    {
        [cell setSearchCell:model];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController * detail = [[DetailViewController alloc] init];
    detail.classId = ((MessageDetailModel *)_dataArray[indexPath.row]).ids;
    [self.navigationController pushViewController:detail animated:YES];
    _selectedIndex = indexPath.row;
}

@end
