//
//  PreViewController.m
//  ForHealth
//
//  Created by qianfeng on 15/10/17.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "PreViewController.h"

@interface PreViewController ()
{
    NSTimer *_timer;
    UIView *_backView;
}
@end
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@implementation PreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _backView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    _backView.tag = 200;
    _backView.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.8 alpha:0.2];
    NSArray *imageNameArray = @[@"baby.jpg",@"美女.jpg",@"牵手2.jpg",@"health"];
    _timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(startImage) userInfo:nil repeats:YES];
    for (int i=0; i<imageNameArray.count; i++)
    {
        UIImage *image = [UIImage imageNamed:imageNameArray[i]];
        NSInteger imageWidth = image.size.width;
        NSInteger imageHeight = image.size.height;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((WIDTH-(WIDTH-40))/2,(HEIGHT-((WIDTH-40)*imageHeight/imageWidth))/2,(WIDTH-40),((WIDTH-40)*imageHeight/imageWidth))];
        imageView.image = image;
        if(i==0)
        {
            [imageView setAlpha:1];
        }
        else
        {
            [imageView setAlpha:0];
        }
        imageView.tag = i+100;
        imageView.clipsToBounds = YES;
        imageView.layer.cornerRadius = 50;
        [_backView addSubview:imageView];
    }
    [self.view addSubview:_backView];
}
-(void)startImage
{
    static NSInteger i=0;
    UIImageView *imageView = (UIImageView *)[self.view viewWithTag:i+100];
    if(i<4)
    {
        [UIView animateWithDuration:1 animations:^{
            [imageView setAlpha:1];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:2 animations:^{
                CGRect imageRect = imageView.frame;
                if(i == 0)
                {
                    imageRect.origin.x = 0;
                    imageRect.origin.y = 0;
                    imageRect.size.width = 0;
                    imageRect.size.height = 0;
                    imageView.frame = imageRect;
                }
                else if(i == 1)
                {
                    imageRect.origin.x = 0;
                    imageRect.origin.y = HEIGHT;
                    imageRect.size.width = 0;
                    imageRect.size.height = 0;
                    imageView.frame = imageRect;
                }
                else if(i == 2)
                {
                    imageRect.origin.x = WIDTH/2;
                    imageRect.origin.y = HEIGHT/2;
                    imageRect.size.width = 0;
                    imageRect.size.height = 0;
                    imageView.frame = imageRect;
                }
                else if(i == 3)
                {
                    if(self.callback)
                    {
                        [UIView animateWithDuration:0.5 animations:^{
                            UIImageView *imageView = (UIImageView *)[self.view viewWithTag:3+100];
                            CGRect imageRect = imageView.frame;
                            imageRect.origin.x = WIDTH;
                            imageRect.origin.y = HEIGHT/2;
                            imageRect.size.width = 0;
                            imageRect.size.height = 0;
                            imageView.frame = imageRect;
                        } completion:^(BOOL finished) {
                            self.callback();
                        }];
                    }
                }
                i++;
            }];
        }];
    }
    else
    {
        [_timer invalidate];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
