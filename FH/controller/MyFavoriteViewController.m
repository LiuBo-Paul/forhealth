//
//  MyFavoriteViewController.m
//  FoodTherapy
//
//  Created by qianfeng on 15/10/15.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "MyFavoriteViewController.h"
#import "DBManager.h"
#import "MessageDetailModel.h"
#import "TableViewCell.h"
#import "DetailViewController.h"
#import "MyFavoriteTableViewCell.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface MyFavoriteViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation MyFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backClick:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(editClick:)];
    _dataArray = [[NSMutableArray alloc] init];
    [self creatData];
    [self creatUI];
}
-(void)backClick:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)editClick:(UIBarButtonItem *)sender
{
    if ([sender.title isEqualToString:@"编辑"])
    {
        _tableView.editing = YES;
        [sender setTitle:@"完成"];
    }
    else if([sender.title isEqualToString:@"完成"])
    {
        _tableView.editing = NO;
        [sender setTitle:@"编辑"];
    }
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        MessageTableModel *model = _dataArray[indexPath.row];
        [[DBManager sharedManager] deleteDataById:model.ids];
        _dataArray = [NSMutableArray arrayWithArray:[[DBManager sharedManager] fetchAllData]];
        [_tableView reloadData];
    }
}
-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [_tableView setEditing:editing animated:animated];
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
//编辑
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_tableView.editing == YES)
    {
        MessageTableModel *model = _dataArray[indexPath.row];
        [[DBManager sharedManager] deleteDataById:model.ids];
        _dataArray = [NSMutableArray arrayWithArray:[[DBManager sharedManager] fetchAllData]];
        [_tableView reloadData];
    }
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
//移动
-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [_dataArray exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    for (int i=0; i<_dataArray.count; i++)
    {
        MessageDetailModel *model = _dataArray[i];
        [[DBManager sharedManager] deleteDataById:model.ids];
        [[DBManager sharedManager] insertFavoriteModel:model];
    }
}
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)creatData
{
    _dataArray = [[NSMutableArray alloc] initWithArray:[[DBManager sharedManager] fetchAllData]];
    if(_dataArray.count == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"还没有收藏" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    }
}
-(void)creatUI
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setSeparatorColor:[UIColor clearColor]];
    [self.view addSubview:_tableView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    MyFavoriteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[NSBundle mainBundle] loadNibNamed:@"MyFavoriteTableViewCell" owner:self options:nil][0];
    }
    MessageDetailModel *model = _dataArray[indexPath.row];
    [cell setMyFavoriteCell:model];
    [cell setBackgroundColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.2 alpha:0.05]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_tableView.editing == NO)
    {
        DetailViewController *controller = [[DetailViewController alloc] init];
        controller.classId = ((MessageTableModel *)_dataArray[indexPath.row]).ids;
        [controller.navigationItem.rightBarButtonItem setTitle:@""];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
