//
//  ClassesTableViewController.m
//  FH
//
//  Created by qianfeng on 15/10/20.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "ClassesTableViewController.h"
#import "CollectionReusableView.h"
#import "RequestDataCenter.h"
#import "MessageClassTablemodel.h"
#import "CachesCenter.h"
#import "SearchViewController.h"
@interface ClassesTableViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_dataArray;
    NSUserDefaults *_defaults;
    CachesCenter *_caches;
    UISearchBar *_searchBar;
    UITableView *_tableView;
    NSMutableArray *_iconArray;
}
@end
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@implementation ClassesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [[NSMutableArray alloc] init];
    [self.view setBackgroundColor:[UIColor blackColor]];
    CGRect rect = self.view.frame;
    rect.size.width = 0;
    rect.size.height = 0;
    rect.origin.y = 40;
    rect.origin.x = 0;
    self.view.frame = rect;
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    if(_defaults == nil)
    {
        _defaults = [[NSUserDefaults alloc] init];
    }
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    _defaults = [NSUserDefaults standardUserDefaults];
    _caches = [[CachesCenter alloc] init];
    UINavigationItem *item = self.navigationItem;
    UIImageView *titleView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleView.clipsToBounds = YES;
    titleView.layer.cornerRadius = 10;
    titleView.image = [UIImage imageNamed:@"titleBackGround"];
    UILabel *titleLable = [[UILabel alloc] initWithFrame:titleView.frame];
    [titleLable setTextAlignment:NSTextAlignmentCenter];
    [titleLable setText:@"分类搜搜"];
    [titleView addSubview:titleLable];
    [item setTitleView:titleView];
    NSData *data = [_caches getDataByFileName:@"loreclass"];
    if(data)
    {
        NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if(dic1)
        {
            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:dic1[@"yi18"]];
            for (NSDictionary *dict in array)
            {
                MessageClassTablemodel *model = [[MessageClassTablemodel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [_dataArray addObject:model];
            }
            [_tableView reloadData];
        }
        else
        {
            NSLog(@"缓存数据有问题!");
        }
    }
    else
    {
        [self requestData];
    }
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2, HEIGHT-64-49)];
    [_tableView setBackgroundColor:[UIColor blackColor]];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH/2, 30)];
    [lable setText:@"分类搜索"];
    [lable setFont:[UIFont systemFontOfSize:17]];
    [lable setTextColor:[UIColor whiteColor]];
    [_tableView setTableHeaderView:lable];
    [self.view addSubview:_tableView];
    _iconArray = [[NSMutableArray alloc] initWithArray:@[@"iconfont-bookmark-1",@"iconfont-bookmark-2",@"iconfont-bookmark-3",@"iconfont-bookmark-4",@"iconfont-bookmark-5",@"iconfont-bookmark-6",@"iconfont-bookmark-7",@"iconfont-bookmark-8",@"iconfont-bookmark-9",@"iconfont-bookmark-10",@"iconfont-bookmark-11",@"iconfont-bookmark-12",@"iconfont-bookmark-13"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadFinished" object:nil];
    self.navigationController.tabBarController.tabBar.hidden = YES;
}
-(void)requestData
{
    NSString *path = @"http://a.apix.cn/yi18/lore/loreclass";
    [[RequestDataCenter sharedRequest] getDataFromUrlString:path];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveData:) name:@"downloadFinished" object:nil];
}
-(void)receiveData:(NSNotification *)notify
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic = [NSMutableDictionary dictionaryWithDictionary:notify.userInfo];
    [_caches saveData:dic[@"data"] WithFileName:@"loreclass"];
    NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:dic[@"data"] options:NSJSONReadingMutableContainers error:nil];
    if(dic1)
    {
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:dic1[@"yi18"]];
        for (NSDictionary *dict in array)
        {
            MessageClassTablemodel *model = [[MessageClassTablemodel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            [_dataArray addObject:model];
        }
        [_tableView reloadData];
    }
    else
    {
        NSLog(@"接收数据有问题!");
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [cell setBackgroundColor:[UIColor blackColor]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    MessageClassTablemodel *model = [[MessageClassTablemodel alloc] init];
    model = _dataArray[indexPath.row];
    [cell.textLabel setFont:[UIFont systemFontOfSize:15]];
    cell.textLabel.text = model.name;
    cell.imageView.image = [UIImage imageNamed: _iconArray[indexPath.row]];
    UISwipeGestureRecognizer *swip = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handelLeft:)];
    [tableView setSeparatorColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.4 alpha:0.1]];
    swip.direction = UISwipeGestureRecognizerDirectionLeft;
    [cell addGestureRecognizer:swip];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    if(self.callback)
    {
        self.callback(searchViewController,((MessageClassTablemodel *)_dataArray[indexPath.row]).name,@"1");
    }
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)handelLeft:(UISwipeGestureRecognizer *)swip
{
    if(self.swipBack)
    {
        self.swipBack();
    }
}
@end
