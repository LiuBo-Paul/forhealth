//
//  RootTableViewController.m
//  FH
//
//  Created by qianfeng on 15/10/20.
//  Copyright (c) 2015年 qianfeng. All rights reserved.
//

#import "RootTableViewController.h"

#import "RequestDataCenter.h"
#import "TableViewCell.h"
#import "MessageTableModel.h"
#import "DetailViewController.h"
#import "CachesCenter.h"
#import "MJRefresh.h"
#import "ClassesTableViewController.h"
#import "OtherViewController.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@interface RootTableViewController ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UISearchBarDelegate>
{
    __block UITableView *_tableView;
    NSMutableArray *_dataArray;
    CachesCenter *_caches;
    __block BOOL _isFirstTime;
    BOOL _isReFresh;
    NSUserDefaults *_defaults;
    UIActivityIndicatorView *_indicator;
    ClassesTableViewController *_classTableViewController;
    CGRect _backRect;
    UIView *_backView;
    UISearchBar *_searchBar;
    NSInteger _selectedIndex;
}
@end
#define URL @"http://a.apix.cn/yi18/lore/list?page=%@&limit=%@&type=%@"
@implementation RootTableViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    _selectedIndex = -1;
    _backView = [[UIView alloc] initWithFrame:CGRectMake(-WIDTH/2, 0, WIDTH*3/2, HEIGHT)];
    _backRect = _backView.frame;
    [self.view addSubview:_backView];
    CGRect rect = self.tabBarController.tabBar.frame;
    rect.origin.y = HEIGHT;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor grayColor]];
    CGRect rectTabBar = self.tabBarController.tabBar.frame;
    rectTabBar.origin.y = HEIGHT;
    self.tabBarController.tabBar.frame = rect;
    self.hidesBottomBarWhenPushed = YES;
    UIImage *image = [UIImage imageNamed:@"tab_class"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(leftBarBtnClick:)];
    UIImage *image1 = [UIImage imageNamed:@"tab_setting"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStyleDone target:self action:@selector(rightBtnClick:)];
    _caches = [[CachesCenter alloc] init];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UINavigationItem *item = self.navigationItem;
    UIImageView *titleView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleView.clipsToBounds = YES;
    titleView.layer.cornerRadius = 10;
    titleView.image = [UIImage imageNamed:@"titleBackGround"];
    UILabel *titleLable = [[UILabel alloc] initWithFrame:titleView.frame];
    [titleLable setTextAlignment:NSTextAlignmentCenter];
    [titleLable setText:@"资讯墙"];
    [titleView addSubview:titleLable];
    [item setTitleView:titleView];
    if(!_defaults)
    {
        _defaults = [[NSUserDefaults alloc] init];
    }
    else
    {
        _defaults = [NSUserDefaults standardUserDefaults];
    }
    NSString *str = [NSString stringWithFormat:@"%@", [_defaults objectForKey:@"page"]];
    _page = [str isEqualToString:@"(null)"]?@"1":str;
    _type = @"id";
    _limit = @"20";
    _isFirstTime = YES;
    _isReFresh = NO;
    _dataArray = [[NSMutableArray alloc] init];
    [self start];
    __weak typeof(self) weakSelf = self;
    __weak typeof(_tableView) weakTableView = _tableView;
    __weak typeof(_classTableViewController) weakClassTableViewController = _classTableViewController;
    __block typeof(_backRect) weakBackRect = _backRect;
    __weak typeof(_backView) weakBackView = _backView;
    _classTableViewController = [[ClassesTableViewController alloc] init];
    _classTableViewController.callback = ^(SearchViewController *searchViewController, NSString *keyWord, NSString *page){
        searchViewController.keyWord = keyWord;
        searchViewController.page = page;
        [weakSelf.navigationController pushViewController:searchViewController animated:YES];
    };
    _classTableViewController.swipBack = ^(){
        [UIView animateWithDuration:0.4 animations:^{
            _backRect.origin.x = -WIDTH/2;
            weakTableView.alpha = 1;
            weakTableView.userInteractionEnabled = YES;
            CGRect rect = weakClassTableViewController.view.frame;
            rect.origin.x = 0;
            rect.origin.y = 0;
            rect.size.width = 0;
            rect.size.height = 0;
            CGRect rect1 = weakTableView.frame;
            rect1.size.height = HEIGHT-64;
            rect1.origin.y = 64;
            weakTableView.frame = rect1;
            weakClassTableViewController.view.frame = rect;
            weakBackView.frame = weakBackRect;
        }];
    };
    
}

-(void)leftBarBtnClick:(UIBarButtonItem *)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        //侧滑
        if(_backRect.origin.x == -WIDTH/2)
        {
            [_classTableViewController.view removeFromSuperview];
            _backRect.origin.x = 0;
            _tableView.alpha = 0.5;
            _tableView.userInteractionEnabled = NO;
            [_backView addSubview:_classTableViewController.view];
            [_backView setBackgroundColor:[UIColor blackColor]];
            CGRect rect = _classTableViewController.view.frame;
            rect.origin.x = 0;
            rect.origin.y = 64;
            rect.size.width = WIDTH/2;
            rect.size.height = HEIGHT-64;
            CGRect rect1 = _tableView.frame;
            rect1.size.height = HEIGHT-64-120;
            rect1.origin.y = 120;
            _tableView.frame = rect1;
            _classTableViewController.view.frame = rect;
        }
        else
        {
            _backRect.origin.x = -WIDTH/2;
            _tableView.alpha = 1;
            _tableView.userInteractionEnabled = YES;
            CGRect rect = _classTableViewController.view.frame;
            rect.origin.x = 0;
            rect.origin.y = 0;
            rect.size.width = 0;
            rect.size.height = 0;
            CGRect rect1 = _tableView.frame;
            rect1.size.height = HEIGHT-64;
            rect1.origin.y = 64;
            _tableView.frame = rect1;
            _classTableViewController.view.frame = rect;
        }
        _backView.frame = _backRect;
        
    }];
}
-(void)rightBtnClick:(UIBarButtonItem *)sender
{
    OtherViewController *other = [[OtherViewController alloc] init];
    [self.navigationController pushViewController:other animated:YES];
}
-(void)start
{
    _indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((WIDTH-40)/2, (HEIGHT-100)/2, 40, 40)];
    [self.view addSubview:_indicator];
    _indicator.hidden = NO;
    [_indicator setColor:[UIColor lightGrayColor]];
    [_indicator startAnimating];
    [self requestData];
    [self creatUI];
}
-(BOOL)isExistModel:(MessageTableModel *)model InArray:(NSMutableArray *)arr
{
    for (int i=0; i<arr.count; i++)
    {
        MessageTableModel *arrmodel = arr[i];
        if(arrmodel.title == model.title)
        {
            return YES;
        }
    }
    return NO;
}
-(void)creatUI
{
    __weak typeof(self) weakSelf = self;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH/2, 64, WIDTH, HEIGHT-64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setSeparatorColor:[UIColor clearColor]];
    _tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.page = [NSString stringWithFormat:@"%d",1];
            [weakSelf requestData];
            _isFirstTime = YES;
            _isReFresh = YES;
            [_tableView.header endRefreshing];
        });
    }];
    _tableView.header.autoChangeAlpha = YES;
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.page = [NSString stringWithFormat:@"%d", [weakSelf.page integerValue] + 1];
            [_defaults setObject:weakSelf.page forKey:@"page"];
            [weakSelf requestData];
            _isFirstTime = YES;
            [_tableView.footer endRefreshing];
        });
    }];
    _tableView.hidden = YES;
    _tableView.footer.autoChangeAlpha = YES;
    [_backView addSubview:_tableView];
    [_backView bringSubviewToFront:_indicator];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    [view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0.3 alpha:0.1]];
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(3, 5, WIDTH-56, 30)];
    _searchBar.delegate = self;
    _searchBar.placeholder = @"搜一搜!";
    _searchBar.tag = 100;
    _searchBar.searchBarStyle = UISearchBarStyleMinimal;
    _searchBar.enablesReturnKeyAutomatically = YES;
    _searchBar.searchResultsButtonSelected = YES;
    [_searchBar setBarTintColor:[UIColor whiteColor]];
    _searchBar.clipsToBounds = YES;
    _searchBar.delegate = self;
    _searchBar.layer.cornerRadius = 5;
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH-50, 5, 47, 30)];
    [btn addTarget:self action:@selector(searchClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"titleBackground"]]];
    btn.clipsToBounds = YES;
    btn.layer.cornerRadius = 5;
    [btn setTitle:@"搜索" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [view addSubview:_searchBar];
    [view addSubview:btn];
    view.userInteractionEnabled = YES;
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
-(void)searchClick:(UIButton *)sender
{
    if(![_searchBar.text isEqualToString:@""])
    {
        SearchViewController *searchView = [[SearchViewController alloc] init];
        searchView.keyWord = _searchBar.text;
        searchView.page = @"1";
        [self.navigationController pushViewController:searchView animated:YES];
        NSMutableData *data = [[NSMutableData alloc] init];
        [_caches saveData:data WithFileName:@"history"];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"搜索内容不能为空,请重新输入!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    }
    [_searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchBar.text.length == 0)
    {
        [searchBar resignFirstResponder];
    }
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
-(void)requestData
{
    [[RequestDataCenter sharedRequest] getDataFromUrlString:[[NSString stringWithFormat:URL,_page,_limit,_type] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveData:) name:@"downloadFinished" object:nil];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.tabBarController.tabBar.hidden = NO;
    if(_selectedIndex == -1)
    {
        [_tableView reloadData];
    }
    else
    {
        NSIndexPath *indexpath = [NSIndexPath indexPathForItem:_selectedIndex inSection:0];
        [_tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.tabBarController.tabBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.tabBarController.tabBar.hidden = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadFinished" object:nil];
}
-(void)receiveData:(NSNotification *)notify
{
    if(_isFirstTime)
    {
        [_indicator stopAnimating];
        _tableView.hidden = NO;
        [_indicator removeFromSuperview];
        if(_isReFresh)
        {
            [_dataArray removeAllObjects];
            _isReFresh = NO;
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic = [NSMutableDictionary dictionaryWithDictionary:notify.userInfo];
        [_caches saveData:dic[@"data"] WithFileName:@"table"];
        NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:dic[@"data"] options:NSJSONReadingMutableContainers error:nil];
        if(dic1)
        {
            NSArray *array = (NSArray *)dic1[@"yi18"];
            for (NSDictionary *dic2 in array)
            {
                MessageTableModel *model = [[MessageTableModel alloc] init];
                [model setValuesForKeysWithDictionary:dic2];
                [_dataArray addObject:model];
            }
            [_tableView reloadData];
        }
        else
        {
            NSLog(@"接收数据有问题!");
        }
        _isFirstTime = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[NSBundle mainBundle] loadNibNamed:@"TableViewCell" owner:nil options:nil][0];
    }
    MessageTableModel *model = [[MessageTableModel alloc] init];
    model = _dataArray[indexPath.row];
    if(model)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell setTableViewCell:model];
                [cell setBackgroundColor:[UIColor colorWithRed:0.2 green:0.1 blue:0.1 alpha:0.05]];
            });
        });
    }
    cell.clipsToBounds = YES;
    cell.layer.cornerRadius = 10;
    UISwipeGestureRecognizer *swip = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handelRight:)];
    swip.direction = UISwipeGestureRecognizerDirectionRight;
    [cell addGestureRecognizer:swip];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 79;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController *detail = [[DetailViewController alloc] init];
    detail.classId =((MessageTableModel *) _dataArray[indexPath.row]).ids;
    [self.navigationController pushViewController:detail animated:YES];
    _selectedIndex = indexPath.row;
}
-(void)handelRight:(UISwipeGestureRecognizer *)swip
{
    [UIView animateWithDuration:0.4 animations:^{
        [_classTableViewController.view removeFromSuperview];
        _backRect.origin.x = 0;
        _tableView.alpha = 0.5;
        _tableView.userInteractionEnabled = NO;
        [_backView setBackgroundColor:[UIColor blackColor]];
        [_backView addSubview:_classTableViewController.view];
        CGRect rect = _classTableViewController.view.frame;
        rect.origin.x = 0;
        rect.origin.y = 64;
        rect.size.width = WIDTH/2;
        rect.size.height = HEIGHT-64-49;
        _classTableViewController.view.frame = rect;
        _backView.frame = _backRect;
        CGRect rect1 = _tableView.frame;
        rect1.size.height = HEIGHT-64-120;
        rect1.origin.y = 120;
        _tableView.frame = rect1;
    }];
}

@end
